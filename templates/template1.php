<?php
function CallAPI($method, $url, $data = false){
           $curl = curl_init();
           switch ($method)
           {
               case "POST":
                   curl_setopt($curl, CURLOPT_POST, 1);
                   if ($data)
                       curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                   break;
               case "PUT":
                   curl_setopt($curl, CURLOPT_PUT, 1);
                   break;
               default:
                   if ($data)
                       $url = sprintf("%s?%s", $url, http_build_query($data));
           }
           // Authentication:
           curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
           // curl_setopt($curl, CURLOPT_USERPWD, "username:password");
           curl_setopt($curl, CURLOPT_URL, $url);
           curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
           curl_setopt($curl, CURLOPT_HTTPHEADER, array(
               'Content-Type: application/json',
               'token: c03e6c9f-b884-4987-b083-9df941041ad0'
               ));
           $result = curl_exec($curl);
           curl_close($curl);
           return $result;
       }
// $response = CallAPI('GET', 'http://default-environment.dnvv7k2pbr.ap-south-1.elasticbeanstalk.com/tripleplay/customers/'.$customerId);
$response = CallAPI('GET', 'http://ekyc.tripleplay.in:8090/tripleplay/customers/'.$customerId);
$json = json_decode($response, true);

// var_dump($json);die;
// echo $json['error'];die;
if($json['error']){
	$res['status'] = $json['error'];
	$res['message'] = $json['message'];
}else{
	$res['status'] = true;
	$res['message'] = 'Success';
}

// print($response);
// print_r($json);
foreach($json['data'] as $dataArray){
	$name = $dataArray['name'];
	$additionalName = $dataArray['additionalName'];
	$jointName = $dataArray['jointName'];
	$jointRelation = $dataArray['jointRelation'];
	$installationAddress = $dataArray['installationAddress'];
	$customerDetails = $dataArray['metaInfo']['customerJson']['customerDetails'];
	$billingAddress = $customerDetails['billingAddress'];
	$phoneNumber = $dataArray['phoneNumber'];
	$altPhoneNumber = $dataArray['altPhoneNumber'];
	$email = $dataArray['email'];
	$designation = $dataArray['designation'];
	$customerJson = $dataArray['metaInfo']['customerJson'];
	$orderPaymentDetails = $customerJson['orderPaymentDetails'];
	$documents = $customerJson['documents'];
	
	$signatureUrl = "";
	if(isset($customerJson['signatureUrl'])){
		$signatureUrl = $customerJson['signatureUrl'];
	}
	// print_r($signatureUrl);
	// $orderBillType = "";
	// if(isset($orderPaymentDetails['billType'])){
		$orderBillType = "E-Bill";
	// }
	// init customer detailsw variables
	// $customerDetails = $dataArray['metaInfo']['customerJson']['customerDetails'];
	$customerTypeSelected = $customerDetails['customerType'];
	// $userImageUrl = "https://i.imgur.com/17PEIZU.jpg";
	$imageLogo = "https://infodetails.s3.ap-south-1.amazonaws.com/logo.png";
	// echo sizeof($customerTypeSelected);
	$autorizedDesignation = "";
	if(isset($customerDetails['autorizedDesignation'])){
		$autorizedDesignation = $customerDetails['autorizedDesignation'];
	}
	
	$customerFatherName = $customerDetails['fatherName'];
	$customerBirthDate = $customerDetails['birthDate'];
	// $customerGender = $customerDetails['Gender'];
	$customerGender = "";
	if(isset($customerDetails['Gender'])){
		$customerGender = $customerDetails['Gender'];
	}
	$customerNationality = $customerDetails['nationality'];
	$customerAadharNo = $customerDetails['aadharNo'];
	$customerRegisteredNumber = $customerDetails['registeredNumber'];
	$customerPropertyType = $customerDetails['propertyType'];

	$customerName = $customerDetails['customerName'];
	$customerFirstName = $customerName['firstName'];
	$customerMiddleName = $customerName['middleName'];
	$customerLastName = $customerName['lastName'];

	$emailId = "";
	if(isset($customerName['emailId'])){
		$emailId = $customerName['emailId'];
	}
	$mobileNumber = "";
	if(isset($customerName['mobileNumber'])){
		$mobileNumber = $customerName['mobileNumber'];
	}

	if(isset($customerName['imageUrl'])){
		$userImageUrl = $customerName['imageUrl'];
	}
	// printprint_r($userImageUrl);
	if(isset($customerName['imageUrl'])){
		$userImageUrl = $customerName['imageUrl'];
	}
	$AuthorizedFirstName = "";
	$AuthorizedMiddleName = "";
	$AuthorizedLastName ="";
	if(isset($customerDetails['authorizedName'])){
		$authorizedName = $customerDetails['authorizedName'];
		$AuthorizedFirstName = $authorizedName['firstName'];
		$AuthorizedMiddleName = $authorizedName['middleName'];
		$AuthorizedLastName = $authorizedName['lastName'];
	}

		$coordinatorFirstName ="";
		$coordinatorMiddleName = "";
		$coordinatorLastName = "";
		$coordinatorMobileNumber = "";
		$coordinatorEmailId = "";
		if(isset($customerDetails['coordinatorName'])){
			$coordinatorName = $customerDetails['coordinatorName'];
			$coordinatorFirstName = $coordinatorName['firstName'];
			$coordinatorMiddleName = $coordinatorName['middleName'];
			$coordinatorLastName = $coordinatorName['lastName'];
			// $coordinatorMobileNumber = $coordinatorName['mobileNumber'];
			// $coordinatorEmailId = $coordinatorName['emailId'];
		}

		$customerAddress = "";
		$customerMobileNo = "";
		$customerPanNo = "";
		$customerPassportNo = "";
	$customerPassportValidUpto = "";
	$customerVisaType ="";
	$customerVisaValidUpto = "";
	$localFirstName = "";
		$localMiddleName = "";
		$localLastName = "";
		if(isset($customerDetails['localReferenceDetails'])){
			$localReferenceDetails = $customerDetails['localReferenceDetails'];
			$customerAddress = $localReferenceDetails['address'];
			$customerMobileNo = $localReferenceDetails['mobileNo'];
			$customerPanNo = $localReferenceDetails['panNo'];
			if(isset($localReferenceDetails['passportDetails'])){
				$customerLocalReferenceDetails = $localReferenceDetails['passportDetails'];
				$customerPassportNo = $customerLocalReferenceDetails['passportNo'];
				$customerPassportValidUpto = $customerLocalReferenceDetails['passportValidUpto'];
				$customerVisaType = $customerLocalReferenceDetails['visaType'];
				$customerVisaValidUpto = $customerLocalReferenceDetails['visaValidUpto'];
			}
			if(isset($localReferenceDetails['name'])){
				$localFirstName = $name['firstName'];
				$localMiddleName = $name['middleName'];
				$localLastName = $name['lastName'];
			}
		}
			$localAddressLine1 = "";
			$localAddressLine2 = "";
			$billingCity = "";
			$billingLandmark = "";
			$billingState = "";
			$billingPincode = "";
			$billingContactNo = "";
		if(isset($customerDetails['billingAddress'])){
			// $localAddressLine1 = $billingAddress['addressLine1'];
			// $localAddressLine2 = $billingAddress['addressLine2'];
			$billingCity = $billingAddress['city'];
			$billingLandmark = $billingAddress['landmark'];
			$billingState = $billingAddress['state'];
			$billingPincode = $billingAddress['pinCode'];
			// $billingContactNo = $billingAddress['contactNo'];
	}

	$orderTypeofservice = $orderPaymentDetails['serviceType'];
	$orderApplicationType = $orderPaymentDetails['applicationType'];

	$planName = "";
	$planAddonValue = "";
	$planValidity = "";
	if(isset($orderPaymentDetails['planDetails'])){
		$planDetails = $orderPaymentDetails['planDetails'];
		$planName = $planDetails['name'];
		// $planAddonValue = $planDetails['addonValue'];
		$planValidity = $planDetails['validity'];
	}

	$paymentRegistrationCharges = "";
	$paymentActiveCharges = "";
	$paymentSecurityDeposit = "";
	$paymentPlanCharge = "";
	$paymentTotal = "";
	$paymentPaymentMode = "";
	$paymentReceiptNo = "";
	$paymentDate = "";
	$paymentChequeDD = "";
	$paymentBank = "";
	$paymentBranch = "";
	$paymentCity = "";
	$paymentOnlinePayment = "";
	$paymentTransactionID = "";
	if(isset($orderPaymentDetails['paymentDetails'])){
		$paymentDetails = $orderPaymentDetails['paymentDetails'];
		if(isset($paymentDetails['registrationCharges'])){
			$paymentRegistrationCharges = $paymentDetails['registrationCharges'];
		}
		if(isset($paymentDetails['activeCharges'])){
			$paymentActiveCharges = $paymentDetails['activeCharges'];
		}
		if(isset($paymentDetails['securityDeposit'])){
			$paymentSecurityDeposit = $paymentDetails['securityDeposit'];
		}
		if(isset($paymentDetails['planCharge'])){
			$paymentPlanCharge = $paymentDetails['planCharge'];
		}
		if(isset($paymentDetails['receiptNo'])){
			$paymentReceiptNo = $paymentDetails['receiptNo'];
		}
		if(isset($paymentDetails['date'])){
			$paymentDate = $paymentDetails['date'];
		}
		if(isset($paymentDetails['chequeDD'])){
			$paymentChequeDD = $paymentDetails['chequeDD'];
		}
		if(isset($paymentDetails['bank'])){
			$paymentBank = $paymentDetails['bank'];
		}
		if(isset($paymentDetails['branch'])){
			$paymentBranch = $paymentDetails['branch'];
		}
		if(isset($paymentDetails['city'])){
			$paymentCity = $paymentDetails['city'];
		}
		if(isset($paymentDetails['transactionID'])){
			$paymentTransactionID = $paymentDetails['transactionID'];
		}
		$paymentTotal = $paymentDetails['total'];
		$paymentPaymentMode = $paymentDetails['paymentMode'];
	}
	$documentNameAndAddress = "";
	$documentAmountOfTransaction = "";
	$documentParticularOfTransaction = "";
	$documentAssessedTax = "";
	$documentLastIncomeReturn = "";
	$documentRegistrationNumber = "";
	$documentCountriesAddress = "";
	$documentVerification = "";
	$documentDate = "";
	$documentPlace = "";
	$documentIdProof = $documents['idProof'];
	$documentAddressProof = $documents['addressProof'];
	if(isset($documents['otherDetails'])){
		$documentDetails = $documents['otherDetails'];
		$documentNameAndAddress = $documentDetails['nameAndAddress'];
		$documentAmountOfTransaction = $documentDetails['amountOfTransaction'];
		$documentParticularOfTransaction = $documentDetails['particularOfTransaction'];
		$documentAssessedTax = $documentDetails['assessedTax'];
		$documentLastIncomeReturn = $documentDetails['lastIncomeReturn'];
		$documentRegistrationNumber = $documentDetails['registrationNumber'];
		$documentCountriesAddress = $documentDetails['countriesAddress'];
		$documentVerification = $documentDetails['verification'];
		$documentDate = $documentDetails['date'];
		$documentPlace = $documentDetails['place'];
	}

	$serviceTypeSelected = "";
	$serviceRefferalCode = "";
	$serviceCafSignDate = "";
	$serviceSignatureSR = "";
	$serviceEkycTXNID = "";
	$serviceEkycTimeStamp = "";
	$serviceAgentAuth­TXNID = "";
	$serviceAgentAuth­TS = "";
	if(isset($customerJson['serviceDetails'])){
		$customerJsonServiceDetail = $customerJson['serviceDetails'];
		// $serviceTypeSelected = $customerJsonServiceDetail['serviceType'];
		$serviceTypeSelected = "FTHL";
		$serviceRefferalCode = $customerJsonServiceDetail['refferalCode'];
		$serviceCafSignDate = $customerJsonServiceDetail['cafSignDate'];
		$serviceSignatureSR = $customerJsonServiceDetail['signatureSR'];
		$serviceEkycTXNID = $customerJsonServiceDetail['ekycTXNID'];
		$serviceEkycTimeStamp = $customerJsonServiceDetail['ekycTimeStamp'];
		$serviceAgentAuth­TXNID = $customerJsonServiceDetail['agentEkycTxnId'];
		$serviceAgentAuth­TS = $customerJsonServiceDetail['agentEkycTs'];
	}
}	
?>

<style>
.headerImg{
	width: 100px; float: left; margin-right: 80px; margin-left:-50px; margin-top:-35px;
}
.mainHeader{
	float: left; width: 350px; text-align:center; margin-top:-40px;
}
.headerP{
	font-size:20px; font-size: 18px; margin-left: -50px;
}
.headerSubHead{
	font-size: 12px; margin-left: -50px; margin-top: -14px;
}
.headerCaf{
	font-size: 11px; margin-top: -11px; margin-left: 26px;
}
.headerCon{
	float: left; width: 280px; margin-top:-40px; margin-left:40px;
}
.float-left{
	float: left;
}
.firstLine{
	background-color: #232020; border-radius: 3px; margin-top:-0px; margin-left:-30px; padding-bottom:8px; line-height:12px;
}
.online{
	text-align: center; font-size:10px; color: white;
}
.ulRow
{
	float: left; width: 100%; list-style: none; padding: 0; margin: 0; margin-top: 10px;
}
.ulRow li{ 
	float: left; width: 165px; margin-right: 10px;
}
.ulRow li input{
	width: 100%; float: left; margin: 0; height: 22px;
}
.ulRow li p{
	width: 100%; text-align: center; font-size: 11px;
}
.ulRow .asd-box{
	width: 514px; height: 22px; border: 1px solid darkgray;
}
.ulRow .email-box{
	width: 282px; height: 22px; border: 1px solid darkgray;
}
.ulRow .birth-box{
	width: 80px; height: 22px; border: 1px solid darkgray;
}
.ulRow .nationality-box{
	width: 80px; height: 22px; border: 1px solid darkgray;
}
.ulRow .passport-box{
	width: 96px; height: 22px; border: 1px solid darkgray;
}
.ulRow .passport-valid-box{
	width: 70px; height: 22px; border: 1px solid darkgray;
}
.ulIndividual{
	float: left; font-size:11px; width: 130px; height: 15px; 
	border: 1px solid darkgray;
}

.ulName{
	float: left; text-align:center; font-size:9px; width: 130px; margin-right: 10px; height: 13px; 
}
.nameInput{
	float: left; text-align:center; font-size:9px; width: 150px; margin-right: 10px; height: 13px; 
}
.ulRowOne{
	float: left; width: 100%; list-style: none; padding: 0; margin: 0; margin-top: 10px;
}
.box{
	width:170px;
	background-color:rgb(245, 245, 245);
	border: 1px solid darkgray;
}
.passport-box{
	width: 90px; height: 15px; border: 1px solid darkgray;
}
</style>

<page style="width:100%;">
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td>
					<div class="headerImg">
						<img style="margin-left:2px; margin-top:12px;" src="<?php print($imageLogo); ?>" width="135px;" height="60px;">
					</div>
				</td>
				<td>
					<div class="mainHeader">
						<p class="headerP"><strong>Customer Agreement Form</strong></p>
						<p class="headerSubHead"><strong>(To be filled in block letters)</strong></p>
					</div>
				</td>
				<td>
					<div class="headerCon">
						<p style="font-size: 11px;">Contact Center: 9308080808</p>
						<p class="headerCaf" style="">CAF Number: 21000091</p>
					</div>
				</td>
			</tr>
					
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div class="firstLine">
						<p class="online">Customer Details</p>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="padding-left:-20px;">
					<div style="font-size: 10px; float: left; margin-left: -10px;">
						<?php for ($i=0; $i < sizeOf($customerTypeSelected) ; $i++) { ?> 
							<input style="float: left;" type="checkbox"
							<?php if($customerTypeSelected[$i]['selected']) echo "checked='checked'"; ?> >
							<span style="margin-right: 8px; font-size: 10px; float: left;">
								<?php print_r($customerTypeSelected[$i]['type']); ?>
							</span>
						<?php } ?>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<table class="float-left" style="width:300px;">
		<tbody class="float-left" style="width:300px;">
			
			<tr class="float-left" >
				<td class="float-left" style="width: 110px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">1.Individual/Organization:</p>
					</div>
				</td>
				<td class="ulIndividual">
					<?php print($customerFirstName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($customerMiddleName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($customerLastName); ?>
				</td>
				<td style="margin-top: 10px; width: 150px;" rowspan="7">
					<img src="<?php print($userImageUrl); ?>" style="width:150px;height:150px;float:right" />
				</td>
			</tr>
			<tr class="float-left" style="margin-bottom:10px;">
				<td class="float-left" style="width: 130px;">
					<p style="font-size:8px; margin-left: -24px; margin-top:-15px;"></p>
				</td>
				<td class="ulName">
					<p style="margin-top:-10px;">First name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Middle name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Last name</p>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 110px;">
					<div style="text-align: center; font-size: 10px; margin-left: -60px;">
						<p style="margin-top: -2px;">Authorised Signatory Name:</p>
					</div>
				</td>
				<td class="ulIndividual">
					<?php print($AuthorizedFirstName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($AuthorizedMiddleName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($AuthorizedLastName); ?>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<p style="font-size:8px; margin-left: -24px; margin-top:-15px;">(NA for Individual/Proprietorship)</p>
				</td>
				<td class="ulName">
					<p style="margin-top:-10px;">First name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Middle name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Last name</p>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<div style="text-align: center; font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">Authorised Signatory Designation:</p>
					</div>
				</td>
				<td class="ulIndividual">
					<?php print($autorizedDesignation); ?>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 110px;">
					<div style="text-align: center; font-size: 10px; margin-left: -60px;">
						<p style="margin-top: -2px;">Organisation Co­ordinator Name:</p>
					</div>
				</td>
				<td class="ulIndividual">
					<?php print($coordinatorFirstName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($coordinatorMiddleName); ?>
				</td>
				<td class="ulIndividual">
					<?php print($coordinatorLastName); ?>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<p style="font-size:8px; margin-left: -24px; margin-top:-15px;">(NA for Individual/Proprietorship)</p>
				</td>
				<td class="ulName">
					<p style="margin-top:-10px;">First name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Middle name</p>
				</td>
				<td class="ulName">
				<p style="margin-top:-10px;">Last name</p>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px;">
						<p style="text-align: center; margin-top: -2px;">Mobile No:</p>
					</div>
				</td>
				<td class="ulIndividual" style="margin-right:20px;">
				<?php print($mobileNumber);  ?> <!-- print($coordinatorMobileNumber);  -->
				</td>
				<td class="float-left" style="width: 60px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">E­mail ID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:205px;">
					<?php  ?> <!-- print($coordinatorEmailId); -->
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 130px;"></td>
				<td class="ulName"></td>
				<td class="ulName" style="width: 280px;">
					<p style="margin-top:-10px; font-size:7px;">(Your postpaid bill/Statements will be maild on above mentioned E­mail ID)</p>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left" style="width:300px;">
		<tbody class="float-left" style="width:300px;">
			<tr class="float-left" >
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">2. Father's/Husband's Name:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:583px;">
					<?php print($customerFatherName); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left" style="width:300px;">
		<tbody class="float-left" style="width:300px;">
			<tr class="float-left" >
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">3.Birth Date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:70px;">
					<?php print($customerBirthDate); ?>
				</td>
				<td style="float:left; font-size:10px; width:55px;">
					<p style="text-align:right; margin-top: -2px;">4.Gender:</p>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox" 
						<?php if($customerGender =="Male") echo "checked='checked'"; ?> > <!-- if($customerGender =="Male") echo "checked='checked'"; -->
					</div>
				</td>
				<td class="float-left" style="width: 10px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-6px;">M</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($customerGender =="Female") echo "checked='checked'"; ?> > <!-- if($customerGender =="Female") echo "checked='checked'"; -->
					</div>
				</td>
				<td class="float-left" style="width: 10px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">F</p>
					</div>
				</td>
				<td class="float-left" style="width: 47px;">
					<div style="font-size: 10px; margin-left: -10px;">
						<p style="margin-top: -2px;">5.Nationality:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:100px;">
					<?php print($customerNationality); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 600px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							6. Details of Local Reference (For Outstation/Foreign National)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left" style="padding-top:10px;">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 28px; ">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">Passport No:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:200px;">
					<?php print($customerPassportNo); ?>
				</td>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">Passport Valid Upto:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:200px;">
					<?php print($customerPassportValidUpto); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 600px;">
					<div style="font-size: 6px; margin-left: -30px;">
						<p style="margin-top: -2px;">
						(Passport & Visa copy required in case of Foreign national Visa
						Validity should not be less than 1 month)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 28px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">Visa type:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:200px;">
					<?php print($customerVisaType); ?>
				</td>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">Visa Valid Upto:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:200px;">
					<?php print($customerVisaValidUpto); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left" style="width:300px; padding-top:10px;">
		<tbody class="float-left" style="width:300px;">
			<tr class="float-left">
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<div style="text-align: left; font-size: 10px;">
						<p style="margin-top: 4px;">Name:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 188px;">
					<?php print($localFirstName); ?>
				</td>
				<td class="ulIndividual" style="width: 188px;">
					<?php print($localMiddleName); ?>
				</td>
				<td class="ulIndividual" style="width: 188px;">
					<?php print($localLastName); ?>
				</td>
			</tr>
			<tr class="float-left">
				<td class="float-left" style="width: 130px;"></td>
				<td class="ulName" style="width: 188px;">
					<p style="margin-top:-10px;">First name</p>
				</td>
				<td class="ulName" style="width: 188px;">
				<p style="margin-top:-10px;">Middle name</p>
				</td>
				<td class="ulName" style="width: 188px;">
				<p style="margin-top:-10px;">Last name</p>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px;">
						<p style="text-align: left; margin-top: -2px;">Address:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 368px;">
					<?php print($customerAddress); ?>
				</td>
				<td class="float-left" style="width: 60px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Mobile No:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:140px;">
					<?php print($customerMobileNo); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; text-align: left;">
						<p style="margin-top: -2px;">PAN/GIR No:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:584px;">
					<?php print($customerPanNo); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 600px;">
					<div style="font-size: 8px;">
						<p style="margin-top: -2px;">
							(Please provide copy of PAN/GIR No. or fill form 60/61)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<!-- <td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">8. Aadhar No:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 200px;">
					
				</td> -->
				<td class="float-left" style="width: 188px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">8. Registered Mobile Number (RMN):</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:180px;">
					<?php print($customerRegisteredNumber); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
			<td class="float-left" style="width: 350px;"></td>
				<td class="float-left" style="width: 355px;">
					<div style="font-size: 8px; margin-left:15px;">
						<p style="margin-top: -2px;">
							(RMN is the Number through which you can do your transactions 
							via SMS/Call/Web Application)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; text-align: left; margin-left: -30px;">
						<p style="margin-top: -2px;">9. Installation & billing Address:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:584px;">
					<?php print($billingCity); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 600px;">
					<div style="font-size: 8px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							(Billing Address required only for postpaid Customers)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">City/Village/PO:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 168px;">
					<?php print($billingCity); ?>
				</td>
				<td class="float-left" style="width: 80px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Landmark:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:167px;">
					<?php print($billingLandmark); ?>
				</td>
				<td class="float-left" style="width: 60px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">State:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:167px;">
					<?php print($billingState); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Pincode:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 80px;">
					<?php print($billingPincode); ?>
				</td>
				<td class="float-left" style="width:168px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Contact No.with STD Code(if any):</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:167px;">
				<?php print($customerRegisteredNumber);  ?> <!-- print($billingContactNo);  -->
				</td>
				<td class="float-left" style="width: 109px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Property Details: </p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
						<?php if($customerPropertyType =="Rented") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 30px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-6px;">Rented</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($customerPropertyType =="Owned") echo "checked='checked'"; ?>	>
					</div>
				</td>
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">Owned</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left" style="margin-top:10px;">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 725px;">
					<div class="firstLine">
						<p class="online">Order & Payment Details</p>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">10. Type of Service:</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
						<?php if($orderTypeofservice =="Pre-paid") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 30px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-6px;">Pre­paid</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($orderTypeofservice != "Pre-paid") echo "checked='checked'"; ?>	>
					</div>
				</td>
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">Post­paid</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">11. Plan Name:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 160px;">
					<?php print($planName); ?>
				</td>
				<td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Add­on DUL value:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:161px;">
					<?php print($planAddonValue); ?>
				</td>
				<td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Validity(Months):</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:120px;">
					<?php print($planValidity); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
			<td class="float-left" style="width: 545px;"></td>
				<td class="float-left" style="width: 120px;">
					<div style="font-size: 8px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							(Applicable only for PrePaid Customers)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 65px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">12. Bill Preference:</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox" 
						<?php if($orderBillType =="Paper Bill") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-6px;">Paper Bill</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($orderBillType =="E-Bill") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">E­-Bill</p>
					</div>
				</td>
				<td class="float-left" style="width: 250px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: center; margin-top: -2px;">13. Type of Application/Usage of Service:</p>
					</div>
				</td>


				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox" 
						<?php if($orderApplicationType =="Own Use") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-6px;">Own Use</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($orderApplicationType =="Cyber Cafe") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 44px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">Cyber Cafe</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($orderApplicationType =="Resale") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-13px;">Resale</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 120px;">
					<div style="font-size: 8px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							(Applicable only for PrePaid Customers)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 150px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							14. Payment Information (Amount in RS)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" style="">
				<td class="float-left" style="width: 70px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Plan Charges:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 348px;">
					<?php print($paymentRegistrationCharges); ?>
				</td>
				<td class="float-left" style="width: 110px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Active Charges:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:170px;">
					<?php print($paymentActiveCharges); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 120px;">
					<div style="font-size: 9px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							(Adjustable in first bill/period)
						</p>
					</div>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 50px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Security Deposit:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 160px;">
					<?php print($paymentSecurityDeposit); ?>
				</td>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Other Charges:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:161px;">
					<?php print($paymentPlanCharge); ?>
				</td>
				<td class="float-left" style="width: 70px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Total:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:110px;">
					<?php print($paymentTotal); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Payment Mode:</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
						<?php if($paymentPaymentMode =="Cash") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 110px;">
					<div style="font-size: 10px;">
						<p style="margin-top: 0px; margin-left:-6px;">Cash Receipt No,</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:120px;">
					<?php print($paymentReceiptNo); ?>
				</td>
				<td class="float-left" style="width: 70px;">
					<div style="font-size: 10px;">
						<p style="text-align:right; margin-top: 0px; margin-left:-6px;">Date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:80px;">
					<?php print($paymentDate); ?>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
						<?php if($paymentPaymentMode =="Cheque") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 50px;">
					<div style="font-size: 10px;">
						<p style="margin-top: 0px; margin-left:-6px;">Cheque/DD</p>
					</div>
				</td>
				<td class="float-left" style="width: 65px;">
					<div style="font-size: 10px;">
						<p style="text-align:right; margin-top: 0px; margin-left:-6px;">Date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:102px;">
					<?php print($paymentDate); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 12px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Bank:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 80px;">
					<?php print($paymentBank); ?>
				</td>
				<td class="float-left" style="width: 40px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Branch:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:70px;">
					<?php print($paymentBranch); ?>
				</td>
				<td class="float-left" style="width: 30px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">City:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:80px;">
					<?php print($paymentCity); ?>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-3px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
					    <?php if($paymentPaymentMode =="Online") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 68px;">
					<div style="font-size: 10px;">
						<p style="margin-top: 0px; margin-left:-6px;">Online Payment</p>
					</div>
				</td>
				<td class="float-left" style="width: 63px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Transaction ID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:80px;">
					<?php print($customerMobileNo); ?>
				</td>
				<td class="float-left" style="width: 26px;">
					<div style="font-size: 10px;">
						<p style="text-align:right; margin-top: 0px; margin-left:-6px;">Date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:78px;">
					<?php print($customerMobileNo); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left" style="margin-top:10px;">
		<tbody class="float-left">
			<tr class="float-left" >
				<td class="float-left" style="width: 725px;">
					<div class="firstLine">
						<p class="online">Documents</p>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">15. Documents Provided (ID proof and address proof are both mandatory)</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 20px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">ID Proof:</p>
					</div>
				</td>
				<td class="float-left" style="width:600px;">
					<div style="font-size: 10px; float: left; margin-left: -10px;">
						<?php for ($i=0; $i < sizeOf($documentIdProof) ; $i++) { ?>
							<input style="float: left;" type="checkbox"
							<?php if($documentIdProof[$i]['selected']) echo "checked='checked'"; ?> > 
							<span style="margin-right: 8px; font-size: 10px; float: left;">
								<?php print_r($documentIdProof[$i]['name']); ?>
							</span>
							
						<?php } ?>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 45px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -12px;">Address Proof:</p>
					</div>
				</td>
				<td class="float-left" style="width:500px;">
					<div style="font-size: 10px; float: left; margin-left: -10px;">
						<?php for ($i=0; $i < sizeOf($documentAddressProof) ; $i++) { ?> 
							<input style="float: left;" type="checkbox"
							<?php if($documentAddressProof[$i]['selected']) echo "checked='checked'"; ?> >
							<span style="margin-right: 8px; font-size: 10px; float: left;">
								<?php print_r($documentAddressProof[$i]['name']); ?>
							</span>
							
						<?php } ?>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>
<!--	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Documents should not be older than 3 months</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						From 60/61 : (from of declaration to be filled by a person who does not have either permanent account number/ general index register number and who makes payment in cash in
						respect of transaction specified in clause (a) to (h) of rule 1148
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left" style="width:300px;">
		<tbody class="float-left" style="width:300px;">
			<tr class="float-left" >
				<td class="float-left" style="width: 137px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">a) Full Name & Address of Applicant:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:576px;">
					<?php print($documentNameAndAddress); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 85px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">b) Amount of transaction:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:260px;">
					<?php print($documentAmountOfTransaction); ?>
				</td>
				<td class="float-left" style="width: 150px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">c) Particular of transaction:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:202px;">
					<?php print($documentParticularOfTransaction); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 130px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">d) Are you assessed to tax (yes/no):</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:107px;">
					<?php print($documentAssessedTax); ?>
				</td>
				<td class="float-left" style="width: 360px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">
							e) If, yes (i) Details of ward/circle/range where the last return of income was field :
						</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:100px;">
					<?php print($documentLastIncomeReturn); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 366px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							(ii)Reasons for not having permanent account number/general index registration number 
						</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:180px;">
					<?php print($documentRegistrationNumber); ?>
				</td>
				<td class="float-left" style="width: 160px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">
							f) Details of documents being
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 146px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							in support of address in countries (a) f)
						</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:119px;">
					<?php print($documentCountriesAddress); ?>
				</td>
				<td class="float-left" style="width: 440px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">
							(Plstick ) I hereby declare that my source of income is from agriculture and i am not required to pay
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
							income tax on any other income. If any (applicable only for Customers with agricultural income).
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 36px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">
							Verification: I
						</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:214px;">
					<?php print($documentVerification); ?>
				</td>
				<td class="float-left" style="width: 455px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">
							do hereby declare that what is stated above is true to the best of my knowledge and belief
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr>
				<td class="float-left" style="width: 65px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="margin-top: -2px;">Verified today, date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:100px;">
					<?php print($documentDate); ?>
				</td>
				<td class="float-left" style="width: 90px;">
					<div style="font-size: 10px;">
						<p style="margin-top: -2px; text-align:right;">place:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:100px;">
					<?php print($documentPlace); ?>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left" style="margin-top:10px;">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div class="firstLine">
						<p class="online">Service Details</p>
					</div>
				</td>
			</tr>	
		</tbody>
	</table>-->
	
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td style="width:20px;">
					<div style="font-size: 12px; float: left; margin-left:-30px; margin-top:-4px;">
						<input style="float: left;" type="checkbox"
						<?php if($serviceTypeSelected =="FTTN") echo "checked='checked'"; ?> >
					</div>
				</td>
				<td class="float-left" style="width: 25px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-40px;">FTTH</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-30px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($serviceTypeSelected != "FTTH/B") echo "checked='checked'"; ?>	>
					</div>
				</td>
				<td class="float-left" style="width: 30px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-40px;">FTTH/B</p>
					</div>
				</td>
				<td style="width:10px;">
					<div style="font-size: 12px; float: left; margin-left:-27px; margin-top:-4px;">
						<input style="margin-left:-7px; float: left;" type="checkbox"
						<?php if($serviceTypeSelected != "RF") echo "checked='checked'"; ?>	>
					</div>
				</td>
				<td class="float-left" style="width: 10px;">
					<div style="font-size: 9px;">
						<p style="margin-top: 0px; margin-left:-37px;">RF</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						have read and understood the terms and conditions mentioned over leaf and unconditionally accept them as binding on me/us. If we have understood all the rates,tariffis and other related conditions at which
						services will be provided in India as applicable as on the data and as amended from limit to time. If we here by undertake to pay all the changes raised on account of services availed. We further declare and
						undertake that the above information provided by me/us is true and condition is respect.
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 70px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Refferal code (if any):</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 160px;">
					<?php print($serviceRefferalCode); ?>
				</td>
				<td class="float-left" style="width: 90px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">CAF sign Date:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:161px;">
					<?php print($serviceCafSignDate); ?>
				</td>
				
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
		<tr class="float-left">
				<td class="float-left" style="width: 50px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Signature of SR:</p>
					</div>
				</td>
				<td  style="width: 200px; padding-top:-12px;">
					<img style="margin-left:2px; margin-top:12px;" src="<?php print($signatureUrl); ?>" width="150px;" height="60px;">
				</td>
			</tr>			
		</tbody>
	</table>
	<!-- <table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 50px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Agent-Auth­TXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 95px;">
					 $serviceAgentAuthTXNID in php code
				</td>
				<td class="float-left" style="width: 80px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">AgentAuth-­TS:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:95px;">
					 $serviceAgentAuthTS in php code
				</td>
				<td class="float-left" style="width: 70px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Ekyc­-TXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:95px;">
					 $serviceEkycTXNID in php code 
				</td>
				<td class="float-left" style="width: 80px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Ekyc-­TimeStamp:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:95px;">
					$serviceEkycTimeStamp in php code 
				</td>
			</tr>			
		</tbody>
	</table> -->
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 48px;">
				<div style="font-size: 10px; margin-left: -30px;">
					
						<p style="text-align: right; margin-top: -2px;">AgentAuth­TXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:130px;">
					<?php print($serviceAgentAuth­TXNID); ?>
				</td>
				<td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">AgentAuth­TS:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:130px;">
					 <?php print($serviceAgentAuth­TS); ?> <!-- print($serviceAgentAuth­TS); -->
				</td>
				<td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">Ekyc­-TXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:130px;">
					<?php print($serviceEkycTXNID); ?>
				</td>
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<!-- <td class="float-left" style="width: 30px;">
				<div style="font-size: 10px; margin-left: -30px;">
					
						<p style="text-align: right; margin-top: -2px;">Ekyc­-TXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:130px;">
					<?php  ?> print($serviceEkycTXNID);
				</td> -->
				<td class="float-left" style="width: 48px;">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">Ekyc-­TimeStamp:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:130px;">
					<?php print($serviceEkycTimeStamp); ?>
				</td>


				<!-- <td class="float-left" style="width: 120px;">
					<div style="font-size: 10px;">
						<p style="text-align: right; margin-top: -2px;">CustomerAuthTXNID:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width: 156px;">
					<?php  ?> print($serviceCustomerAuthTXNID);
				</td> -->
				<!-- <td class="float-left" style="width: 100px;">
					<div style="font-size: 10px; margin-left: -60px;">
						<p style="text-align: right; margin-top: -2px;">CustomerAuth­TS:</p>
					</div>
				</td>
				<td class="ulIndividual" style="width:140px;">
					$serviceCustomerAuthTS in php code
				</td> -->
			</tr>			
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">Terms and Conditions</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						The Terms & Conditions (referred as “Terms”) herein form an integaral part of Customer 
						Agreement Form (CAF) which determines the use ofInternet Services, Applications, Content 
						Services& Value Added Services (collectively referred as “Services”) mentioned herein and 
						signed by the Customer and the same shall be legally binding on the Customer. The relevant 
						Acts, Regulations and Rules of Government of India and Telecom Regulatory Authority of India 
						(TRAI) shall have over riding effect over this Form.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">Acceptance of Terms</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						Customer can accept the Terms by signing the Customer Agreement Form (hereinafter 
						referred as CAF)or by clicking to accept or agree to the Terms, where this option is
						 made available to Customer, on a website or web application for any service. Also, 
						 if a Customer starts using the Services, then Customer’s use of Services will be 
						 treated as acceptance of the Terms.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">Definitions</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 735px;">
					<ul style="margin-left:-36px; margin-top:-10px; font-size:8px;">
						<li style="width: 725px;">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"TP NetworksPvt. Ltd." also referred as "Service Provider" or "TP", who has signeda Licence Agreement withDepartment of Telecommunications (DOT), Ministry of Communications and Information Technology, Government of India, for providing Internet Service in All India Service Area (hereinafter called as “Licensed Service Area”).
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Customer" means any person or legal entity who has agreed to avail the Services, under these Terms & Conditions mentioned in this Customer Agreement Form (CAF).
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Customer Agreement Form or CAF"means this form that has to be filled and executed by any person or legal entity for becoming a Customer, and includes these Terms & Conditions.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Customer Premise Equipment" (CPE) includes Modem/ Router/ or any other equipment installed at customer premises, whether owned by customer or not.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Customer Premises" means the address and location mentioned in the CAF by Customer for using the Service(s).
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Dot" means Department of Telecommunication, Ministry of Communications and Information Technology.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Post Paid" means a method of payment where a Customer pays for the Services and usage in arrears, subsequent to consuming the services.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Pre­Paid" means a mode of payment for Services where a customer pays a set amount in advance of actual usage.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Roaming Service" means customer access to Wi­Fi at multiple locations or area where service is offered by Service Provider or any partnerwith whom Service Provider has arrangements.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Service(s)" means all Internet access, Content services, Internet Telephony and other additional Value­Added/ Supplementary services, as opted for by Customer.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Tariff" means rates, fees, charges, etc. payable by Customer for Services and related conditions at which Services maybe provided, including Deposits, Activation & Installation Fees, Rentals, Fixed Charges, Usage Charges, any other related fees or service charges, and also includes applicable taxes like Service Tax, GST. Tariff shall have the same meaning as contained in Telecommunication Orders issued by TRAI.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"TRAI" means Telecom Regulatory Authority of India.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							"Wi­Fi", short for Wireless Fidelity, is a technology that allows an electronic device to connect to an Internet Access Point wirelessly (without any need for wires, using permitted radio waves) and access Internet, Internet Content, and exchange data.
							</p>
						</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">1.Provision of Services</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						1.1 TP reserves the right to verify the details/ documents provided by Customer and seek/ verify other information including financial information from independent sources as deemed necessary.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						1.2 TP reserves the right to reject any form or application, for any reason, without any liability whatsoever. In case of non acceptance/ rejections, refund, wherever applicable, will be made according to regulations from time to time. information provided by Customer or gathered by TP shall become the property of TP even if the form/ application is rejected.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						1.3 Service shall be provisioned and provided within a reasonable time after receipt and acceptance of CAF and subject to technical feasibility. TP will connect equipment to the network and will use all reasonable endeavours to maintain connection and provide service throughout the enrolment period.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						1.4 The period of subscription shall commence upon activation of the Services, post TP accepting, after due verification, the Customer Agreement Form (CAF) duly signed by Customer and shall run in concurrence with SP’s License Agreement with DOT and shall be subject to all applicable laws, rules, regulations, notifications, order, directions of the Government/ Courts/ Tribunal/ TRAI/ Indian Telegraph Act 1885, besides being subject to the terms of this CAF.
						</p>
						<p style="text-align: left; margin-top: -2px;">	
						1.5 TP shall have the right to fix and/ or change credit limit for the usage of the services. TP shall be entitled to seek interim payments and/ or bar the availability of Services upon reaching the credit limit.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						1.6 TP shall have the right to fix and/ or change fair usage limit for the usage of the services. TP shall be entitled to rate limit or bar the availability of Services upon reaching the defined fair usage limit.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						1.7 Quality, functionality and/ or availability of services, may be affected and TP, without any liability, whatsoever, is entitled to refuse, limit, suspend, vary or disconnect the services, at any time, for reasonable cause, including, but not limited to
						</p>		
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 735px;">
					<ul style="margin-left:-36px; margin-top:-10px; font-size:8px;">
						<li style="width: 725px;">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Government/ DoT/ TRAI rules, regulations, orders, directions, notifications etc, prohibiting and/ or suspending the rendering of such services or otherwise affecting such service directly, indirectly or consequentially.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Any violation by the Customer, of applicable rules, regulations, orders, directions, notifications, etc. issued by Government/ DoT/ TRAI, or of the terms and conditons contained herein.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Any discrepancy/ wrong particular(s) provided by Customer.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Upon delay/ non­payment of Tariff charges beyond the due date, TP reserves the right to disconnect the service either totally or partially without any prior notice.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							In case Customer exceeds pre­fixed usage or credit limit in relation to Customer's usage of Service(s).
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							During technical failure, modification, upgradation or variation, relocation, repair and/ or maintenance of the system/ equipment.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							To combat potential fraud, sabotage, wilful destruction, national security related issues etc.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Due to acts of God or circumstances beyond the control of TP including insurrection or civil disorder, or military operation, national or local emergency, industrial disputes of any kind, fire, lightening, explosion, flood, inclement weather conditions, acts of omission or commission of person or of persons or bodies for whom TP is not responsible.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Transmission limitations caused by topographical, geographical, atmospheric, hydrological, mechanical, electronic conditions or constraints.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Interconnection between TP and other service providers.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Privacy of communication is subject to Government's regulations/ rules/ directions (in force from time to time) and any such other factors including but not limited to, matter relating to national security and defence.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							TP may block Internet sites, fully or party, and/ or individual customers, as identified and directed by the Statutory Authorities or Security Agencies from time to time.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Any other reason/ cause which is found to be reasonable by TP warranting limiting/ suspension/ disconnection.
							</p>
						</li>
					</ul>
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						1.8 Customer Aknowledge that TP have Right  to send SMS/Notification directly or indirectly regarding or related to Tripleplay Service/Billing/Offer/Payment or if any.
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">2. Service Tariffs and Procedures</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						2.1 Different plans/ schemes, including the additional or supplementary serviecs shall have correspondingly tariff and terms & conditions, all of which are subject to change by TP from time to time, within the overall guidelines of TRAI and that Customer shall not have any claim or right in such eventuality.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						2.2 TP is entitled to add/ alter or withdraw any additional or supplementary Services including their tariff and terms & conditions at any time, in its sole direction. Terms and conditons for provision of additional/ supplementary Services shall be deemed to be an integral part of this CAF.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						2.3 The procedure for metering of usage shall be determined and/ or varied by TP from time to time.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						2.4 All taxes, present and future, and additional taxes/ cess/ duties etc that may be levied by Government/ Local Authorities etc with respect to Services will be charged to the Customer's account.
						</p>		
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">3.Billing and Payment</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						3.1 The billing/ charging cycle shall run on monthly basis or such other frequency as may be decided by TP from time to time. Customer is responsible to pay his/ her dues to TP by the prescribed date,as applicable for the relevant Tariff Plan or as mentioned in the bills/ invoices/ statements issued by TP.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.2 Bills/ Statements is available at customer account level and shall be sent electronically to the email address and/ or billing address indicated by the Customer in the CAF, or as amended. Any change in Billing Address shall be communicated by Customer as per prescribed procedure and TP reserves the right to verify the change of address before acknowledging the change in its records.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.3 Payment through modes like Electronic means/ Cash/ Cheque/ DD/ Pay Order should be made at the payment center or other means as may be communicated by TP from time to time. Outstation Cheque not at par, Postal Order or Money Orders will not be accepted.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.4 Customer owns the liability for the cheques submitted. Dishonouring of any cheque shall invite penalty on Customer as decided by TP and without prejudice to the statutory (civil and criminal) remedies available to TP under the law.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.5 Customer shall pay all dues in full, without any deduction, set­off or withholding whatsoever, in respect of all Services availed, whether or not these exceed the assigned credit limit.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.6 In case a Customer contends that he/she/ it has been billed wrongly, then Customer shall pay the amount outstanding against the said bill/ statement and raise a claim separately to prove the contention within 30 days of the disputed bill/ statement date. After necessary examination and scrutiny.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.7 TP shall be entitled to seek interim payments and/ or bar the availability of Services upon reaching and/ or exceeding the credit limit/ deposits.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.8 Payments against the dues, if made beyond the due date, shall entail late Payment fees of Rs. 100 or 2% per month of the Amount Due, whichever is higher or as may be decided by TP from time to time. This, however, is without prejudice to TP’s right to suspend or disconnect the Services partially or 
						fully due to Non­Payment. If a Customer is under temporary disconnection and does not apply for reconnection within a stipulated time post paying all outstanding amount, then TP reserves the right to permanently terminate the service. If a Customer is under Permanent Disconnection and decides to resume 
						the service, Customer will have to pay all outstanding amounts and apply for fresh activation of Service(s) by signing a new CAF with applicable activation and other charges.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						3.9 Customer shall deposit amount(s) such as Security Deposit or Advance Payment to TP as decided from time to time as part of the Tariff Plan. TP reserves the right to forfeit/ adjust/ apply the said security deposit amount in full or in part satisfaction of any sum due from Customer at any time. 
						However, No interest shall be paid on the deposit(s). TP reserves the right to withhold the amount of deposit(s)/ advance(s) etc. at any time in its sole discretion. TP may call for advance/ additional security deposit for Services requested by Customer.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<!-- <table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
							3.9 Customer shall deposit amount(s) such as Security Deposit or Advance Payment to SP as decided from time to time as part of the Tariff Plan. SP reserves the right to
							forfeit/ adjust/ apply the said security deposit amount in full or in part satisfaction of any sum due from Customer at any time. However, Customer shall continue to be liable
							for such sum due to SP. No interest shall be paid on the deposit(s). SP reserves the right to withhold the amount of deposit(s)/ advance(s) etc. at any time in its sole
							discretion. SP may call for advance/ additional security deposit for Services requested by Customer.
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table> -->
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">4. Customer's Undertakings and Commitments</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						4.1 Customer undertakes and agrees to to pay the due bills/ chargesby the due date in favour of TP as per the Tariff plan opted from time to time. It is Customer’s responsibility to inquire about his dues from TP at the beginning/ end of each billing/ charging cycle & settle the same even in case of non­receipt of bills or statements. Customer acknowledges that delay or default in payment would be a material breach of the terms and conditions governing the provision and delivery of Services.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.2 Customer shall and undertakes to fully comply with all applicable laws, bye­laws, rules, regulations, order, direction, notification, etc., including without limitation, to the provisions of the Indian Telegraph Act 1885, TRAI Act,and any amendments or replacements made thereto, issued by the Government/ Court/ Tribunals/ TRAI and shall also comply with all the directions issued by TP which relate to the network, Service, equipment, or connected matter. Customer shall provide TP with all the co­operation that it may reasonably require from time to time in this regard.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.3 Customer acknowledges that TP’s acceptance of payment from a person other than Customer will not amount to TP having transferred or subrogated any of its rights or obligation of Customer to such third party.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.4 Customer understands and agrees that IP addresses allocated to Customer through TP are non­portable and shall continue to remain the exclusive property of SP.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.5 Customer undertakes that the services provided by TP shall not be used for purposes other than the purposes declared herein and the purpose is permissible under the
						applicable statutory or regulatory provisions issued by DOT or TRAI.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.6 Customer agrees to comply with the 'Prohibitory Clauses' detailed later.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.7 Registration of DoT under the Other Service Provider (OSP Category): As per guidelines issue by DoT from time to time, OSPs such as Call Centres (both international & domestic), Network Operations Centres, Vehicle Tracking Systems, Tele­Banking, Tele­Medicine, Tele­Trading, E­Commerce, shall have to be registered with DoT for their respective services and location of operations (for details, refer to www.dot.gov.in). Persons intending to avail TPservices for providing the said services must first furnish a copy of Registration Certificate issued to them by DoT along with CAF.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.8 In the event Wi­Fi network is installed in Customer’s Premises, then (a) It will be informed to TP,(b) Customer undertakes to use secured Wi­Fi network connection to avoid any misuse, (c) Customer undertakes that he shall be solely responsible for any use/ misuse of any Wi­Fi installation,(d) Customer is required to set up and maintain its own authentication mechanism for its internet usage/ Wi­Fi Services,(e) Customer undertakes to keep a log of all the events on Wi­Fi network for a period of at least one year and shall provide the same to the regulatory and/ or security agencies.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.9 TP shall have the right to inspect the installation/ billing address of the connection and Customer shall provide all the necessary support and access to TP for the same.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.10 It would be Customer’s responsibility to ensure that passwords/ CPE Informationis kept confidential. TP shall not be held liable for misuse of Customer's Services/ Equipment under any circumstances including but not limited to misuse on account of access by third parties to such confidential password/ CPE information.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.11 Customer hereby gives consent to TP to collect, use, share, retain his/ her/ its personally identifiable information, usage information, and billing information, and contact him/ her/ it using such information for all purposes necessary for providing & improving services and for suggesting additional services.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						4.12 While accessing Internet and the Service(s), advertisement/ promotions may be displayed. Customer agrees that he has no objection of any kind or manner to the placement of such advertisement/ promotions etc. while using or availing the said Services.
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">5.Transferability</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						The services/ connection given under this CAF shall normally be non­transferable. However, Customer may seek prior consent from TP for seeking transfer. In case of such request being agreed by TP, the Primary Customer& Transferee shall fulfil all requiredformalities. The primary Customer shall be liable to fully discharge his duties till the date of transfer. The security deposit received from original/ primary Customer will be adjusted/ returned, after deduction (if any) as the case may be. The transferee would have to give fresh security deposit toTP, as applicable.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">6. Shifting Of Connection</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						Customer shall not remove or shift any equipment installed by TP at Customer's premises, where connection is installed. Shifting of connection and Services within Customer Premises or to another location would be subject to technical feasibility. Applicable charges would apply to Customer’srequest for shifting of connection to new location within the premises or to another location.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">7. Service Discontinuation</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						Customer may opt to discontinue the Services by providing a written notice to TP anytime during the billing cycle after paying all billed dues. TP would stop providing the services within 7 days of receipt of request for discontinuation. Customer is liable to pay all outstanding amount and charges till the day of the Service disconnection. TP shall not be liable to the Customer or any third party for any damages, direct or indirect, consequent to such discontinuation of Service by Customer. Upon discontinuation of Services, Customer shall return the equipment(s) provided by TP, and allow TP to remove such equipment(s).
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">8. Ownership of Equipment and Software</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						All equipments and wiring supplied by TP shall always be the property of TP. The Customer shall not claim any lien, charge or any form of encumbrance over such equipment at any time. Customer is responsible to protect such equipment from any theft or damage. In case of theft or damage of TP provided equipment, then Customer agrees to compensate with such amount as may be decided by TP.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						TP grants to Customer and Customer so accepts a non exclusive and non assignable license to use any software provided by TP to access Internet. The title to all such software would be at all point of time continued to vest with TP.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">9.Prohibitory Clauses</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						9.1 Services shall be used only by Customer or persons authorized by him/ her/ it for their own use and shall not be resold in any way whatsoever.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.2 Customer shall not use the Service to send, transmit, download or in any way deal with any objectionable, obscene or pornographic messages or material, which are inconsistent with the established laws of the country.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.3 Customer shall not use the Service for any unlawful, immoral, improper or abusive purpose, or for sending obscene, indecent, threatening, harassing, unsolicited messages or messages affecting/ infringing national security nor create any damage or risk to TP or its network and/ or other customers.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.4 Customer shall not resort to hacking, cracking, spamming, destroying or corrupting any of the sites on the Internet, nor shall indulge in any of the offences, more specifically defined under the Information Technology Act, 2000.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.5 Customer undertakes and agrees to take the required measures to ensure that spam malicious traffic is not generated from customer end. If at any time spam activity ­ unwanted or malicious is observed from the customer link, TP reserves the right to lock/ suspect or terminate the link immediately without any notice.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.6 Title & Intellectual Property Rights of Services & Content provided through the Services are owned by TP, its agents, suppliers or affiliates or their licensors or otherwise owners of such material are protected by copyright laws and treaties. The customer is prohibited from misusing, copying, re­producing, distributing, transmiting, publishing, publicly exhibiting, altering, adapting, customizingamny of the content protected by copyright laws. The content may include technological measures for the protection of the content and to use the content as per permissible usage rules. Any technological measures to alter, amend or change the usage rules embedded into the content in any manner shall result in civil and criminal liability, under the Information Technology Act 2000 or any other applicable law forcopyright protection.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						9.7 Customer shall not use the Service(s) for any of the following activities:
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 735px;">
					<ul style="margin-left:-36px; margin-top:-10px; font-size:8px;">
						<li style="width: 725px;">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Voice communication from anywhere to anywhere by means of dialling a telephone number (PSTN/ ISDN/ PLMN).
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Originating the voice communication service from a Telephone in India, or Terminating the voice communication to Telephone within India.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Establishing connection to any Public Switched Network in India and/ or establishing gateway between Internet & PSTN/ ISDN/ PLMN in India.
							</p>
						</li>
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
							Use of dial up lines with outward dialling facility from nodes.
							</p>
						</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 735px;">
					<ul style="margin-left:-36px; margin-top:-10px; font-size:8px;">
						<li style="width: 735px;padding-top:10px">
							<p style="font-size: 10px; text-align: left; margin-top: 0px;">
								Interconnectivity with other ISPs.
							</p>
						</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
							9.8 Customer may use encryption upto 40 bit key length in the RSA algorithms or its equivalent in other algorithms without obtaining permission. However, if encryption
							equipments higher than this limit are to be deployed, Customer shall do so only with the permission of DOT and deposit the decryption key, split into two parts, with DOT.
						</p>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">10. Privacy and confidentiality</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						TP reserves the right to share Customer's personally identifiable information, and usage/ billing information with the authorised Government bodies and to access information over the network established by Customer, when advised to do by the authorised Government bodies in pursuance of the laws of the land, and as specifically provided for under the the licence agrement of TP with DoT and the Information Technology Act, 2000.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">11. Disclaimer and limitation of liability</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						11.1 Download/ Upload speed indicated is speed up to TP's ISP node. Broadband speed available to Customer is maximum prescribed speed for which Customer is entitled and TP does not hold out any assurance that the said speed shall be maintained at all times and the same may vary depending upon the network congestion, technical reason or any other unavoidable circumstances.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.2 As installation, and provisioning of services require several vital and time consuming activities, including, inter­alia, technical feasibility check, laying down of cable/ fibre, proper wiring of the area/ premises, other technical requirements etc., TP does not prescribe or hold out any fixed timeline for commissioning of Services after execution of the CAF. TP shall endeavour to activate the services within reasonable time on best effort basis and Customer shall not be entitled to any claim or damage of whatsoever nature on account of delay in commissioning of Services, except for refund of the initial amount paid.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.3 TP makes no explicity or implied warranties, or guarantees that the service will be uninterrupted, error free or regarding the Service uptime or quality.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.4 TP offers no warranty that any information, software or other material accessible on the Service is free of viruses, worms, trojan horses or other harmful components.
						</p>
						<p style="text-align: left; margin-top: -2px;">
							11.5 Though every effort is being made to provide best quality of services, SP shall in no event be responsible to Customer or to any third party, for deficiency in data
							transmission or for any inconvenience, damage or loss that may be caused to any one or of any kind arising thereof.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.6 TP will not be responsible for any liability on account of any of the content that may be communicated, disseminated, transmitted, downloaded, stored, either on a permanent or temporary basis, or in any way dealt with by Customer using the Services.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.7 Under no circumstances shall TP be liable for any direct, indirect, incidental, special, punitive or consequential damages that may result in any way from Customer's use of or inability to use the Services or access the Internet or any part thereof, or Customer's reliance on or use of information, service or merchandise provided on or through the Service, or that may result from mistakes, omissions, interruptions, deletion of files, errors, defects, delays in operation or transmission or any failure of performance thereof.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.8 TP shall not be liable to Customer and Customer hereby waives and agrees to continue to waive all claims of anylosses, opportunity loss, fees, expenses, etc. direct, incidental or consequential arising out of any delays, failure or deficiency with respect to Services, as a result of reasons covered in clause 1.7 hereof.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.9 TP will not be responsible for any acts of franchisees, trade partners of any other third parties with regards to any scheme/ plans, which are purported to have been offered on behalf of TP, without explicit authorization in writing from TP. TP shall not be liable for any acts of commission or omission of franchisees, trade partners, or any other third parties offering any privilege or benefit to Customer.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.10 TP shall not be liable for misuse of Customer's user account or equipment under any circumstances including but not limited to misuse on account of access by third parties to confidential password/ CPE information.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						11.11 TP shall not be liable for any claim, loss or damage of whatsoever nature that may arise due to installation or use of equipment, including any installed software, in Customer Premises, that are not provided by TP. TP will have no role, whatsoever, in such equipment's functioning and warranty, etc. and will not be responsible for any defect/ fault etc. at any time.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">12. Amending The Form</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						12.1 TP shall have the option to add, delete, or amend any terms and conditions forming part of this CAF due to administrative and commercial compulsions or for any other reason considered necessary in the interest of business operations. The Terms & Conditions would be updated on the website. Customer's continued use of services or payments to TPeven after such changes will constitute Customer's acceptance of all such amendments.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						12.2 If any part of this Form is held invalid, the remaining provisions will remain unaffected and enforceable, except to extent that TP’s rights/ obligations under the CAF are materially impaired.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">13. Other Matters</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						13.1 Any notification by Customer or TP to the other party shall be given in writing through electronic mail/ or through post/Mobile Notification.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.2 Where two or more persons constitute the Customer, their liability under the Form shall be joint & several.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.3 TP, without notice, may assign allor parts of its obligations, rights, or duties under this CAF to any third party.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.4 The CAF binds the Customer and his heirs, executors, administrators, successors, and permitted assigns, wherever applicable and also binds TP & its successors and assigns.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.5 The information provided overleaf shall be treated as part and parcel of this CAF.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.6 The headings are for convenience sake only and shall not affect the meaning of the provision thereof nor can the provisions be interpreted in the light of such headings.
						</p>
						<p style="text-align: left; margin-top: -2px;">
						13.7 The failure of any party thereto at anytime to require performance by the other party of any obligation/ provision of this CAF shall not affect the right of such party to require performance of that obligation/ provision subsequently. Waiver by any party of breach of any provision/ obligation of this Form shall not be constructed as waiver of any continuing or succeeding breach of such provision, or waiver of the provision, itself or a waiver of any right(s) here under.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">14. Dispute Resolution</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						Any disputes arising between Customer & TP will be referred to a sole Arbitrator to be appointed by TP. The provisions of Indian Arbitration and Conciliation Act, 1996 or amendments thereto shall apply. The Courts in India will have jurisdiction for the purpose of this CAF.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;  margin-top:10px;">
						<b style="text-align: left; margin-top: -2px;">Acceptance By Customer</b>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="float-left">
		<tbody class="float-left">
			<tr class="float-left">
				<td class="float-left" style="width: 725px">
					<div style="font-size: 10px; margin-left: -30px;">
						<p style="text-align: left; margin-top: -2px;">
						Customer has fully read/ has been explained in vernacular, verbatim the terms and conditions of this CAF and confirms that he/ she/ it is fully conversant with the Service(s) provided/ to be provided by TP together with its tariff, specifications, requirements, limitations, etc and has signed this Form knowing and having such understanding. Customer further confirms that he/ she/ it has understood the contents thereof and has signed in token of unconditional acceptance of the aforesaid terms and conditions, with the understanding that this is a valid and binding document and can be enforced in accordance with law.
						</p>	
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</page>			