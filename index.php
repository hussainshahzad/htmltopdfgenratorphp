<?php
header('Content-Type: application/json');
error_reporting(E_ALL & ~E_WARNING);

generatePdf();

function generatePdf(){
    $res = [
        'status' => false,
        'message' => 'Error on generating PDF file.',
        'data' => null
    ];
    if(!isset($_REQUEST['id']) or empty($_REQUEST['id'])){
        $res['message'] = "id param can not be blank";
        echo json_encode($res);
        exit;
    }
    $customerId = $_REQUEST['id'];
    echo $customerId;
    
    // echo json_encode($customerId);
    require_once(dirname(__FILE__).'/vendor/autoload.php');
    try
    {
        ob_start();
        include dirname(__FILE__).'/templates/template1.php';
        $content = ob_get_clean();
        // $content = ob_get_contents();
        ob_clean();
        // ob_end_clean();
        // echo $content;
        if($res['status']){
        //    echo json_encode($res['status']);
        //    echo 'test1';
            // $html2pdf = new HTML2PDF('P', 'A4', 'fr', false, 'UTF-8', array(12, 5, 12, 5));
            // $html2pdf = new HTML2PDF('P', 'A4', 'fr');
            // $html2pdf = new HTML2PDF('L', 'A4', 'en', array(10, 5, 10, 5));
            // $html2pdf =new HTML2PDF('L');
            // $html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
            // $html2pdf->pdf->SetDisplayMode('fullpage');
            
            try{
                echo 'e1';
                $html2pdf = new HTML2PDF('P', 'A4', 'fr', true, 'UTF-8', array(12, 5, 12, 5));
                $html2pdf->pdf->SetDisplayMode('fullpage');
                $html2pdf->writeHTML($content);
                // $html2pdf->writeHTML($content);
                // $html2pdf->writeHTML($_POST["$content"]);
                // $html2pdf->setDefaultFont('Arial');
                // $html2pdf->writeHTML($content);
                echo 'e2';
                $filename = time() . '-pdf.pdf';
            $output_path = '/tmp/' . $filename;
            $html2pdf->Output($output_path, 'F');
            
            echo 'data to upload';
            $uploadPdfRes = uploadPdf($output_path);
            echo 'Uploaded data';
            echo $uploadPdfRes;
            $uploadPdfRes = json_decode($uploadPdfRes, true);
            // echo json_encode($uploadPdfRes);
            if(!$uploadPdfRes['error']){
                $pdfUrl = $uploadPdfRes['data']['url'];
                $res['status'] = true;
                $res['message'] = 'Success';
                $res['data'] = $pdfUrl;
            }else{
                $res['status'] = false;
                $res['message'] = 'Failed to save PDF to Remote location';
            }

            unlink($output_path);
            }
            catch(HTML2PDF_exception $e) {
                echo 'EX'.$e;
                // $res['status'] = false;
                // $res['message'] = 'failed';
            }

            
        }
    }
    catch(HTML2PDF_exception $e) {
        $res['status'] = false;
        $res['message'] = $e;
    }

    // echo json_encode($res);
    exit;
}


function build_data_files($boundary, $fields, $files){
    $data = '';
    $eol = "\r\n";
    $delimiter = '-------------' . $boundary;
    foreach ($fields as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
            . $content . $eol;
    }
    foreach ($files as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="file"; filename="' . $name . '"' . $eol
            . 'Content-Transfer-Encoding: binary'.$eol
            ;
        $data .= $eol;
        $data .= $content . $eol;
    }
    $data .= "--" . $delimiter . "--".$eol;
    return $data;
}

function uploadPdf($filePath){

    $url = "http://default-environment.dnvv7k2pbr.ap-south-1.elasticbeanstalk.com/tripleplay/uploadFile/";
    $fields = array("filetype"=>1);

    // files to upload
    $filenames = array($filePath);

    $files = array();
    foreach ($filenames as $f){
       $files[$f] = file_get_contents($f);
    }

    $url_data = http_build_query($data);

    $boundary = uniqid();
    $delimiter = '-------------' . $boundary;

    $post_data = build_data_files($boundary, $fields, $files);
    
    $curl = curl_init();
    

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => $post_data,
      CURLOPT_HTTPHEADER => array(
        //"Authorization: Bearer $TOKEN",
        "Content-Type: multipart/form-data; boundary=" . $delimiter,
        "Content-Length: " . strlen($post_data)

      ),

      
    ));

    $response = curl_exec($curl);
    return $response;
}

?>
